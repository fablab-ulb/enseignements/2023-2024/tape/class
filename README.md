# Techniques Avancées de Physique Expérimentale TAPE 2022-2023 (partie 1)

Titulaire : Prof. Denis Terwagne  
Cours ouvert aux étudiants en Master en physique et de l'école polytechnique de l'Université Livre de Bruxelles (ULB)

Dans ce cours, l'étudiant apprendra à développer un processus d'exploration technique pour gagner en maîtrise d'un aspect d'un outil scientifique experimental et à le transmettre.

![image du système de microscopie UC2](img/UC2-image.png)

## Etudiants

* [Rodrigue Dedonder](https://gitlab.com/fablab-ulb/enseignements/2023-2024/tape/students/rodrigue.dedonder)

## Exercices

### Ex. 0 - Le journal de bord

Tout au long de ce cours, vous rendrez compte de votre processus d'exploration sous la forme d'un journal de bord qui allie narration et argumentation scientifique.

Commencez le journal en vous présentant brièvement. Jour après jour rendez compte de vos explorations suivant les exercices donnés ci-dessous, décrivez les difficultés rencontrées, vos échecs et comment vous les avez surmontés.

Pour ce faire, vous utiliserez la plate-forme GitLab du cours et des outils de compression d'images tel que [GIMP](https://www.gimp.org/).   
Afin de limiter l'impact sur la planète, la taille maximale de votre espace ne devra pas excéder 25Mo.

### Ex. 1 - Préambule, la caméra de votre smartphone

Caractériser la caméra et la lentille de votre smartphone.  
Transformer votre smartphone en microscope à l'aide d'une goutte d'eau et mesurer la taille d'un cheveux.
Décriver les protocoles, caractéristiques techniques, grossissements...
   
### Ex. 2 - Système de microscopie UC2

Qu'est-ce que le microscope UC2 ? Quelles sont ses capacités, ses possibilités et ses limitations ?


### Ex. 3 - Systèmes de deux lentilles

Décrire, monter et analyser [le montage à 2 lentilles (lunette astronomique)](https://github.com/openUC2/UC2-GIT/tree/master/APPLICATIONS/APP_SIMPLE-Telescope) et ses particularités suivant la distance entre les deux lentilles, la position de l'objet,...

### Ex. 4 - Microscopie UC2 en champs clair pour le smartphone

Imprimer en 3D et monter les modules nécessaires pour compléter et réaliser [le montage du microscope UC2 en champs clair pour le smartphone](https://github.com/openUC2/UC2-GIT/tree/master/APPLICATIONS/APP_SIMPLE-Smartphone_Microscope) (impression 3D + CAD si nécessaire)

### Ex. 5 - Microscopie UC2 en champs sombre pour le smartphone

Adapter et modifier [le dernier montage]() en développant les modules nécessaires (CAD + impressions 3D) pour transformer le microscope en un microscope en champs sombres. Fonctionne-t-il ? Caractérisez-le ?

## Liens utiles

Frugal Science  

* [The Frugal Way, The Economist, 2011](https://gmwgroup.harvard.edu/publications/frugal-way)
* [Open-source lab](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/blob/9ca42c056c4371315ecdac35195d28098be02f0b/vade-mecum/open-source-lab.md)

UC2 Microscope - You see too

* UC2 ([Github](https://github.com/openUC2/UC2-GIT), [Nat. Com. paper](https://www.nature.com/articles/s41467-020-19447-9), [phys.org](https://phys.org/news/2020-11-microscope-open-source-optical-toolbox.html))
    * [UC2 Simple setups](https://github.com/openUC2/UC2-GIT/blob/master/APPLICATIONS/Readme.md#simple-setups)
    * [read more about the projects](https://github.com/openUC2/UC2-GIT#read-more-about-the-project-in-our-papers)


Foldscope  

* [Ted talk : A 50-cent microscope that folds like origami, Manu Prakash](https://www.youtube.com/embed/h8cF5QPPmWU)
* Foldscope: Origami-Based Paper Microscope ([site web](https://foldscope.com/), [PLOS One paper](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781))
* [Communauté Microcosmos](https://microcosmos.foldscope.com/)

Other frugal science tools

* Paperfuge: An ultra-low cost, hand-powered centrifuge inspired by the mechanics of a whirligig toy ([news](https://www.wired.com/2017/01/paperfuge-20-cent-device-transform-health-care/), [video](https://www.youtube.com/watch?v=L5ppD07DMKQ),[paper on bioRxiv](https://www.biorxiv.org/content/10.1101/072207v1))

Outils numériques  

* [GitLab](https://gitlab.com/)
* [Markdown](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown)
* [GIMP](https://www.gimp.org/)
